package can.murat.bm.helloandroid;

import android.util.Log;

import java.net.Inet6Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.Enumeration;

/**
 * Created by Murat on 30.01.2015.
 */
public class NetTools {
    public static void logNetInterfaces(){
        try {
            for (Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces(); en.hasMoreElements();) {
                NetworkInterface intf = en.nextElement();
                Log.v("tag1", "an interface detected!" + intf.toString());
                for (Enumeration<InetAddress> enumIpAddr = intf.getInetAddresses(); enumIpAddr.hasMoreElements();) {
                    InetAddress inetAddress = enumIpAddr.nextElement();
                    if (!inetAddress.isLoopbackAddress()) {
                        if(inetAddress instanceof Inet6Address)
                            Log.v("tag1","an inet6 address detected! "+inetAddress.getHostAddress().toString());
                        else
                            Log.v("tag1","an inet4 address detected! "+inetAddress.getHostAddress().toString());
                    }
                }
            }
        } catch (SocketException e) {
            Log.v("tag1","an acception during net interface info printing");
            e.printStackTrace();
        }
    }
}
