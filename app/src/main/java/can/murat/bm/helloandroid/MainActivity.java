package can.murat.bm.helloandroid;

import android.os.Bundle;
import android.os.Looper;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;


public class MainActivity extends ActionBarActivity
        implements View.OnClickListener {

    private TextView conInfoTextView;
    private TextView massageTextView;

    private Socket socket;
    private static final int serverPort = 6226;
    private static String server_IP = "130.211.138.29";//"192.168.2.31";
    ArrayList<Message> msgList;
    MessageListAdapter msgListAdapter;

    InetAddress localAddress;
    int localPort;
    Thread listenerThread;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(this);
        massageTextView = (TextView) findViewById(R.id.massage_edit_text);

        conInfoTextView = (TextView) findViewById(R.id.con_info_text_view);
        ListView msgHistoryListView = (ListView) findViewById(R.id.message_history_listview);


        msgList = new ArrayList<Message>();
        msgListAdapter = new MessageListAdapter(msgList);
        msgHistoryListView.setAdapter(msgListAdapter);

        listenerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    BufferedReader in = new BufferedReader(
                            new InputStreamReader(
                                    socket.getInputStream()));
                    String line = null;
                    while ((line = in.readLine()) != null) {
                        final String msg=line;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                msgList.add(new Message(null, msg, true));
                                msgListAdapter.notifyDataSetChanged();
                            }
                        });
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        new Thread(new Runnable() {
            @Override
            public void run() {
                Looper.prepare();
                try {
                    InetAddress serverAddr = InetAddress.getByName(server_IP);
                    socket = new Socket(serverAddr, serverPort);
                    Toast.makeText(getApplicationContext(), "Socet Opened!", Toast.LENGTH_SHORT).show();
                    localAddress = socket.getLocalAddress();
                    localPort = socket.getLocalPort();
                    listenerThread.start();

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            conInfoTextView.setText("L.IP:" + localAddress + "    p:" + localPort);
                        }
                    });
                } catch (UnknownHostException e1) {
                    Toast.makeText(getApplicationContext(), "Socet Failed!", Toast.LENGTH_SHORT).show();
                    e1.printStackTrace();
                } catch (IOException e1) {
                    Toast.makeText(getApplicationContext(), "Socet Failed!", Toast.LENGTH_SHORT).show();
                    e1.printStackTrace();
                }
                Looper.loop();
            }
        }).start();
    }

    @Override
    public void onClick(View v) {
        String massage = massageTextView.getText().toString();
        Log.v("dev", massage);
        Log.v("dev", "Local: " + localAddress.toString() + ":" + localPort);
        msgList.add(new Message("Me", massage, false));
        msgListAdapter.notifyDataSetChanged();
        PrintWriter out = null;
        try {
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            out.println(massage);
        } catch (IOException e) {
            e.printStackTrace();
        }

        massageTextView.setText("");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
        try {
            socket.close();
            socket = null;
            Toast.makeText(this, "Socet Closed!", Toast.LENGTH_SHORT).show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

class Message {
    String sender;
    String msgText;
    boolean isReceived;


    Message(String sender, String msgText, boolean isReceived) {
        this.sender = sender;
        this.msgText = msgText;
        this.isReceived = isReceived;
    }

    public String getMsgText() {
        return msgText;
    }

    public String getSender() {
        return sender;
    }

    public boolean isReceived() {
        return isReceived;
    }
}

class MessageListAdapter extends BaseAdapter {
    ArrayList<Message> msgList;

    public MessageListAdapter(ArrayList<Message> msgList) {
        this.msgList = msgList;
    }

    @Override
    public int getCount() {
        return msgList.size();
    }

    @Override
    public Object getItem(int position) {
        return msgList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Message msg = msgList.get(position);
        TextView tv = new TextView(parent.getContext());
        tv.setText(msg.getMsgText());
        if (!msg.isReceived) {
            tv.setGravity(Gravity.RIGHT);
        }

        return tv;
    }
}